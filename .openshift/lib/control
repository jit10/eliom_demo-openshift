#!/usr/bin/env bash

source $OPENSHIFT_CARTRIDGE_SDK_BASH

STOPTIMEOUT=10
FMT="%a %b %d %Y %H:%M:%S GMT%z (%Z)"

exe=${OPENSHIFT_APP_NAME}
pidfile=${OPENSHIFT_OCAML_DIR}ocsigen.pid
logfile=${OPENSHIFT_LOG_DIR}${OPENSHIFT_APP_NAME}.log
config=${OPENSHIFT_DEPENDENCIES_DIR}${OPENSHIFT_APP_NAME}.conf

export OCAMLFIND_CONF=${OPENSHIFT_DEPENDENCIES_DIR}findlib.conf
export LD_LIBRARY_PATH=${OPENSHIFT_DEPENDENCIES_DIR}lib
export PATH=${OPENSHIFT_DEPENDENCIES_DIR}:$PATH

function is_ocsigen_service_running() {
	if [ $(process_running $exe $pidfile; echo $?) -eq 0 ]; then
		return 0
	fi
	return 1
}

function status() {
	if is_ocsigen_service_running; then
		client_result "Application is running"
	else
		client_result "Application is not running"
	fi
}

function start() {
	if is_ocsigen_service_running; then
		echo "`date +"$FMT"`: Application '$OPENSHIFT_APP_NAME' is already running; skipping start()" >> $logfile
		return 0
	fi

	if ! hash $exe 2>/dev/null; then
		client_debug "$exe doesn't exist; invoking build.."
	fi
	if [ -f $config ]; then
		client_result "Modifying config for gear-specific changes.."
		sed -e "s|/\(.*\)app-root/|${OPENSHIFT_HOMEDIR}app-root/|" -e "s|<port>\(.*\)<|<port>${OPENSHIFT_OCAML_IP}:${OPENSHIFT_OCAML_PORT}<|" -i $config
		client_result "Success"

		client_result "starting $exe with config $config.."
		nohup $exe -c $config >> $logfile 2>&1 &
		pid=$!
		ret=$?
		if [ $ret -ne 0 ]; then
			echo "Application '$OPENSHIFT_APP_NAME' failed to start - $ret" 1>&2
			exit $ret
		fi
		echo "$pid" > $pidfile
	else
		client_error "Missing config file $config"
	fi
}

function stop() {
	killall $exe
	echo "`date +"$FMT"`: Stopped Ocaml application '$OPENSHIFT_APP_NAME'" >> $logfile
	rm -f $pidfile
}

# Ensure arguments.
if ! [ $# -eq 1 ]; then
	echo "Usage: $0 [start|stop|status]"
	exit 1
fi

# Handle commands.
case "$1" in
	start)            start          ;;
	stop)             stop           ;;
	status)           status         ;;
	*)                exit 0         ;;
esac

