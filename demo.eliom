{shared{
  open Eliom_lib
  open Eliom_content
  open Html5.D
}}

module Demo_app =
  Eliom_registration.App (
    struct
      let application_name = "demo"
    end)

let main_service =
  Eliom_service.App.service ~path:[] ~get_params:Eliom_parameter.unit ()

let () =
  Eliom_service.register_eliom_module "demo"
  (fun () ->
  Demo_app.register
   ~service:main_service
    (fun () () ->
        Lwt.return
        (Eliom_tools.F.html
           ~title:"demo"
           ~css:[["css";"demo.css"]]
           Html5.F.(body [
               h2 [pcdata "Welcome from Eliom's distillery!"];
               ])))
    )
